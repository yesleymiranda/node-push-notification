"use strict";

const {LoggerAndResponse} = require("../utils/methods");

const router = require("express").Router();

const {_PING} = require("../utils/messages");

/** Ping para verificar disponibilidade da API **/
router.get("/ping", (req, res) => {
  try {
    req.body_response.setMessage(_PING);
    req.body_response.setOk({ping: "pong"}, res);
  } catch (e) {
    LoggerAndResponse(__filename, req, res, e);
  }
});

module.exports = router;
