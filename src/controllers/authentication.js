/* eslint-disable no-undef */
"use strict";

const jwt = require("jsonwebtoken");

const secret = process.env.JWT_SECRET || "senha-dev";
const expiresIn = process.env.JWT_EXPIRES_IN || "1d";

const {isAuthorized} = require("../validations");
const {LoggerAndResponse} = require("../utils/methods");


const _filename = __filename;

const User = require("../models/User");

class PostsController {

  /** Get all entities **/
  async authentication(req, res) {
    try {

      const {username, password} = req.body;

      const entity = await User.findOne({
        where: {
          removed: false, username, password, situation: User.SituationEnum().ATIVO
        },
        attributes: {
          exclude: ["password", "created_at", "updated_at", "removed", "situation"]
        }
      });

      isAuthorized(entity);

      const token = jwt.sign({user: entity}, secret, {expiresIn});

      req.body_response.setOk({access_token: token, expires_in: expiresIn, entity}, res);

    } catch (e) {
      LoggerAndResponse(_filename, req, res, e);
    }
  }

  async verifyJwt(req, res, next) {

    try {

      const token = req.headers.authorization;

      if (!token) {
        req.body_response.setError({a: "Authorization é obrigatório!"});
        return res.status(400).json(req.body_response);
      }

      jwt.verify(token.replace("Bearer ", ""), secret);

      next();

    } catch (e) {

      if (e.name === "TokenExpiredError")
        return LoggerAndResponse(_filename, req, res, {
          status: 401,
          message: e.message
        });

      if (e.name === "JsonWebTokenError")
        return LoggerAndResponse(_filename, req, res, {
          status: 401,
          message: e.message
        });


      LoggerAndResponse(_filename, req, res, e);
    }


  }
}

module.exports = new PostsController();
