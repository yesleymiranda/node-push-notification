const router = require("express").Router();

const {intercept} = require("../../validations");

const validations = require("../../validations/user");
const controller = require("../../controllers/user");

/** GET List all entities **/
router.get("/users", controller.getAll);

/** POST Create new entity **/
router.post("/user", validations.validateNew(), intercept, controller.create);

/** PUT Update entity **/
router.put("/user/:id", validations.validateUpdate(), intercept, controller.update);

/** PUT Update entity **/
router.delete("/user/:id", intercept, controller.delete);

module.exports = router;
