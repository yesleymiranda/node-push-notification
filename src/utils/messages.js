/** Mensageria **/
class Messages {
  messages() {
    return {
      _PING: "API OK =]",
      _API_ERROR: "Falha ao tentar executar o serviço. Tente novamente mais tarde",
      _API_SUCCESS: "Sucesso",
      _API_CREATED: "Criado com sucesso",
      _NOT_AUTHORIZED: "Não autorizado",
      _AUTH_EXPIRED: "Token expirado",
      _NOT_AUTH: "Nome de usuário ou senha inválidos!",

      /** Validations **/
      _INVALID_PARAMS: "Parâmetros inválidos",
      _REQUIRED: "Campo obrigatório",
      _DUPLICATED: "Duplicidade não permitida!",

      /** Database **/
      _DB_INSERT: "Não foi adicionar no momento, tente novamente mais tarde",
      _DB_UPDATE: "Não foi atualizar no momento, tente novamente mais tarde",
      _DB_DELETE: "Não foi deletar no momento, tente novamente mais tarde",
      _NOT_FOUND: " - não localizada(o)",

    };
  }
}

module.exports = new Messages().messages();
