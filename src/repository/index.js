// eslint-disable-next-line no-undef
const Logger = new (require("../utils/logger"))(__filename);

class Repository {

  /**
   * Create entity
   * entityName - (string|object)(required) name ou model ex: 'Clientes' ou const Clientes = require('../models/Clientes')
   * entity - (object)(required) values to create entity ex: { name: 'yesley', idade: 30 }
   * options - (object)(optional) options do sequelize
   * More info:
   * https://sequelize.org/master/class/lib/model.js~Model.html#static-method-create
   */
  async createEntity(model, entity, options) {
    try {

      if (!model) {
        Logger.error("model is required!");
        return false;
      }

      if (!model.create) {
        model = require(`../models/${model}`);
      }

      if (!entity) {
        Logger.error("entity is required!");
        return false;
      }

      return await model.create(entity, options || {});
    } catch (e) {
      Logger.error(`createEntity: ${model}`, e.stack);
      return false;
    }
  }

  async constructorWhere(fields, where = {}) {

    Object.keys(fields).forEach(item => {
      if (item !== "limit" && item !== "offset")
        if (fields[item]) {
          where[item] = fields[item];
        }
    });

    return where;
  }

  /**
   * Update entity
   * @param model - (string)(required) name model ex: 'Clientes'
   * @param values - (object)(required) values to update ex: { name: 'yesley', idade: 30 }
   * @param options - (object)(required) options, where, join, ... to update ex: {where: {cliente_uuid: '242ea894-ea5d-4ef2-a940-6bd5d0219a47'}};
   *
   * More info:
   * https://sequelize.org/master/class/lib/model.js~Model.html#static-method-update
   */
  async updateEntity(model, values, options) {
    try {

      if (!model) {
        Logger.error("model is required!");
        return false;
      }

      if (!model.update) {
        model = require(`../models/${model}`);
      }

      if (!values) {
        Logger.error("values is required!");
        return false;
      }

      if (!options || !options.where) {
        Logger.error("options.where is required!");
        return false;
      }

      const response = await model.update(values, options);
      if (!response || !response[0]) return false;
      return response[0];
    } catch (e) {
      Logger.error(`updateEntity: ${model}`, e.stack);
      return false;
    }
  }
}

module.exports = new Repository();
