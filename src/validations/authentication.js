const {body} = require("express-validator");

class PostsValidations {

  validateAuth() {
    return [
      body(["username", "password"], "required").isLength({min: 6}),
    ];
  }

}

module.exports = new PostsValidations();
