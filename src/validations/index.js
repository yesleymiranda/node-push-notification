const {validationResult} = require("express-validator");

const m = require("../utils/messages");

class Validations {

  /** Retorno 400 - caso algum campo fora das regras **/
  intercept(req, res, next) {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
      let instructions = [];

      errors.array().forEach((item) => {
        if (item.msg === "required") {
          instructions.push(`${item.param} - ${m._REQUIRED}`);
        } else {
          instructions.push(`${item.param} - ${item.msg}`);
        }
      });

      req.body_response.setMessage(m._INVALID_PARAMS);
      req.body_response.setInstructions(instructions);

      return res.status(400).json(req.body_response);
    } else {
      next();
    }
  }

  isFound(entity, entityName) {
    if (!entity)
      throw ({status: 400, message: entityName + m._NOT_FOUND});
  }

  isAuthorized(entity) {
    if (!entity)
      throw ({status: 401, message: m._NOT_AUTH});
  }

  isCreated(entity) {
    if (!entity)
      throw ({status: 400, message: m._DB_INSERT});
  }

  isUpdated(entity) {
    if (!entity)
      throw ({status: 400, message: m._DB_UPDATE});
  }

  isRemoved(entity) {
    if (!entity)
      throw ({status: 400, message: m._DB_DELETE});
  }

}

module.exports = new Validations();
