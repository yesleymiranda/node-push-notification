const {_API_ERROR, _API_SUCCESS, _API_CREATED} = require("../utils/messages");

/** Utilizado para padronizar o retorno da API */
class Response {

  constructor(token_request, execution) {
    this.code = "0000";
    this.message = "";
    this.response = null;
    this.instructions = [];
    this.token_request = token_request;
    this.execution = execution;
  }

  setMessage(message) {
    this.message = message;
  }

  setError(err) {
    this.code = err.code || "9999";
    this.message = err.message || _API_ERROR;
    this.response = err.response || {error: err.parent || err.stack || err};
    return this;
  }

  setInstructions(instructions) {
    this.instructions = instructions || [];
  }

  setOk(body, response, options = {status: 200}) {
    this.code = "0000";
    this.message = _API_SUCCESS;
    this.response = body || {};
    response.status(options.status).json(this);
  }

  setCreated(body, response, options = {status: 201}) {
    this.code = "0000";
    this.message = _API_CREATED;
    this.response = body || {};
    response.status(options.status).json(this);
  }

}

module.exports = Response;
