const {Model, DataTypes, Sequelize} = require("sequelize");

const messages = require("../utils/messages");

class User extends Model {
  static init(sequelize) {
    super.init(
        {
          id: {type: DataTypes.UUID, primaryKey: true, defaultValue: Sequelize.UUIDV4},
          name: DataTypes.STRING,
          last_name: {type: DataTypes.STRING, unique: true},
          username: DataTypes.STRING,
          profiles: DataTypes.STRING,
          profiles_array: {
            type: new DataTypes.VIRTUAL(DataTypes.STRING, ["profiles"]),
            get: function () {
              return this.get("profiles").split(",");
            }
          },
          password: DataTypes.STRING,
          situation: DataTypes.STRING,
          removed: DataTypes.BOOLEAN,
          created_at: DataTypes.DATE,
          updated_at: DataTypes.DATE
        },
        {
          modelName: "user",
          sequelize,
          freezeTableName: true,
        }
    );

  }

  static SituationEnum() {
    return {
      ATIVO: "ATIVO",
      INATIVO: "INATIVO",
      RASCUNHO: "RASCUNHO",
    };
  }

  static async validateDuplicatedUsername(username) {
    const user = await User.findOne({where: {username}});
    if (user) throw ({status: 400, message: `Nome usuário: ${messages._DUPLICATED}`});
  }
}

module.exports = User;
