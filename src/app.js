/* eslint-disable no-undef */
"use strict";

const express = require("express");
const cors = require("cors");
const uuid = require("uuid");

const Logger = new (require("./utils/logger"))(__filename);
const Response = require("./models/Response");
const {verifyJwt} = require("./controllers/authentication");


class App {

  constructor() {
    this.express = express();
    this.middleware();
    this.routes();
  }

  middleware() {
    /** Use body JSON **/
    this.express.use(express.json());

    /** Configurações de cors **/
    this.express.use(cors()); // TODO Close cors

    /** Gera Token por requisição **/
    this.express.use(this.getTokenRequest);

    /** Log no inicio de cada requisição **/
    this.express.use(this.logBegin);

    /** Gera um body para padronizar o response **/
    this.express.use(this.createBodyResponse);

    /** Log ao finalizar cada requisição **/
    this.express.use(this.getLog4js());
  }

  routes() {

    /** Routes without auth **/
    this.express.use("/api/", require("./controllers/ping"));
    this.express.use("/api/", require("./routes/no-auth/authentication"));

    /** Routes with auth **/
    this.express.use("/api/auth/", verifyJwt, require("./routes/auth/user"));

  }

  getTokenRequest(req, res, next) {
    /** Gera um uuid para cada requisição (auditoria) **/
    req.token_request = uuid.v4();
    next();
  }

  logBegin(req, res, next) {
    /** Padronizar de acordo a necessidade de cada sistema **/
    Logger.info(`${req.token_request}   ${req.method}        ==> ${req.path}`);
    Logger.info(`${req.token_request}   Headers      ${JSON.stringify(req.headers).replace(/"/g, "")}`);
    Logger.info(`${req.token_request}   Parametros   ${JSON.stringify(req.query).replace(/"/g, "")}`);
    Logger.info(`${req.token_request}   Body         ${JSON.stringify(req.body)}`);
    next();
  }

  createBodyResponse(req, res, next) {
    /** Cria um body padrão para uso no response  **/
    req.body_response = new Response(req.token_request, new Date());
    next();
  }

  getLog4js() {
    /** Gera log para cada response de req **/
    return Logger.log4js().connectLogger(Logger.logger(), {
      level: "info",
      format: (req, res, format) => format(`[${Logger.replaceFileName()}]  ${req.token_request}   <==   [status: :status]  [:response-time ms]`)
    });
  }
}

module.exports = new App().express;
